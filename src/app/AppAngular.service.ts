import { Injectable } from '@angular/core';
import { Http,RequestOptions } from "@angular/http";
@Injectable()
export class AppAngularService {

  constructor(private htpp:Http) { }
  featchAppAngular(){
    return this.htpp.get("http://localhost:1337/Phonbook").map(res=> res.json())
    
  }
  addPerson(values){
    console.log(JSON.stringify(values))
    return this.htpp.put("http://localhost:1337/Phonbook",values).map(res=> res)
  }
  deletePerson(values){
    console.log(JSON.stringify(values))
    return this.htpp.delete("http://localhost:1337/Phonbook/",new RequestOptions({body: values,})).map(res=> res)
  }
}