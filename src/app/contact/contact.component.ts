import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
 isAddingNewContact:boolean=false;
contact=[];
  constructor() { }

  ngOnInit() {
    this.contact=[
      {
        name: 'Saman hettiyarachi',
        phone: '071216555'
      },
      {
        name: 'Thusitha Nanayakkara',
        phone: '07765656565'
      }
    ]
  }
onSubmit(newcontact){
  this.contact.push(newcontact);
  this.isAddingNewContact=false;
}
}
