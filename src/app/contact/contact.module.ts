import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact.component';
import{ProfileformComponent}from'./profileform/profileform.component';
import{FormsModule}from'@angular/forms';
import { ContmodifiComponent } from './contmodifi/contmodifi.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports:[
    ContactComponent
  ],
  declarations: [ContactComponent,ProfileformComponent, ContmodifiComponent]
})
export class ContactModule { }
