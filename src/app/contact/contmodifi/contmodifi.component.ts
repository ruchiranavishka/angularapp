import { Component, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'contmodifi',
  templateUrl: './contmodifi.component.html',
  styleUrls: ['./contmodifi.component.css']
})
export class ContmodifiComponent implements OnInit {
@Output() onSubmitted:EventEmitter<any> = new EventEmitter();
@Output() onCancel:EventEmitter<any>=new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
onSubmit(newcontact){
this.onSubmitted.emit(newcontact);
}
cancel(){
  this.onCancel.emit();
}
}
