import { TestBed, inject } from '@angular/core/testing';

import { AppAngularService } from './AppAngular.service';

describe('AppAngular', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppAngularService]
    });
  });

  it('should be created', inject([AppAngularService], (service: AppAngularService) => {
    expect(service).toBeTruthy();
  }));
});