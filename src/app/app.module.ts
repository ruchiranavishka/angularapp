import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ContactComponent } from './contact/contact.component';
import { ContactModule } from './contact/contact.module';
import { AppAngularService } from "./AppAngular.service";
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    BrowserModule,
    ContactModule,
    HttpClientModule,
    HttpModule
  ],
  providers: [
    AppAngularService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
